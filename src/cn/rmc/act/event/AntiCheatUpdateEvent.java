package cn.rmc.act.event;

import cn.rmc.act.enums.AntiCheat;
import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;


public class AntiCheatUpdateEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    @Getter
    private final Player player;
    @Getter
    private final AntiCheat[] Anticheats;
    public AntiCheatUpdateEvent(Player p, AntiCheat[] ac){
        this.player = p;
        this.Anticheats = ac;
    }
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
