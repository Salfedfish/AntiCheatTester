package cn.rmc.act;

import cn.rmc.act.enums.AntiCheat;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.permissions.PermissionAttachment;

import java.util.UUID;

public class PlayerData {
    @Getter
    private UUID uuid;
    @Getter
    @Setter
    private Boolean kick;
    @Getter
    @Setter
    private Boolean flag;
    @Getter
    @Setter
    private Boolean damage;
    @Getter
    @Setter
    private Boolean hungry;
    @Getter
    @Setter
    private AntiCheat[] ac;
    @Getter
    @Setter
    private PermissionAttachment permission;
    public String acdisplay(){
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for(AntiCheat acc : ac){
            i++;
            if(i != ac.length){
                sb.append(acc.getName().get(acc.getName().size()-1)+", ");
            }else {
                sb.append(acc.getName().get(acc.getName().size()-1));
            }
        }
        return sb.toString();
    }
    public PlayerData(UUID uuid){
        this.uuid = uuid;
        permission = Bukkit.getPlayer(uuid).addAttachment(main.getInstance());
        kick = false;
        flag = true;
        damage = false;
        hungry = false;
        ac = new AntiCheat[]{AntiCheat.AAC};
    }


}
