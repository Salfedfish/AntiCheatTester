package cn.rmc.act.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class TagUtils {
    public static void setTag(Player p, String prefix, String suffix) {
        if (prefix.length() > 16) {
            prefix = prefix.substring(0, 16);
        }
        if (suffix.length() > 16) {
            suffix = suffix.substring(0, 16);
        }
        for (Player player : Bukkit.getOnlinePlayers()) {
            Scoreboard board = player.getScoreboard();
            Team t = board.getTeam(p.getName());

            if (t == null) {
                t = board.registerNewTeam(p.getName());
                t.setPrefix(prefix);
                t.setSuffix(suffix);
                t.addPlayer(p); continue;
            }
            t.setPrefix(prefix);
            t.setSuffix(suffix);
            if (!t.hasPlayer(p)) {
                t.addPlayer(p);
            }
        }
    }

    public static void unregisterTag(Player p) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            Scoreboard board = p.getScoreboard();
            if (board != null && board.getPlayerTeam(player) != null)
                board.getPlayerTeam(player).unregister();
        }
    }

    public static void unregisterAll() {
        for (Player o : Bukkit.getOnlinePlayers())
            unregisterTag(o);
    }
}
