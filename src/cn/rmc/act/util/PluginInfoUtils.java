package cn.rmc.act.util;

import cn.rmc.act.enums.AntiCheat;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

public class PluginInfoUtils {
    public static String getVersion(String plugin){
        Plugin p = Bukkit.getPluginManager().getPlugin(plugin);
        if(p == null) return "unknown";
        return p.getDescription().getVersion();
    }
    public static String getAuthor(String plugin){
        Plugin p = Bukkit.getPluginManager().getPlugin(plugin);
        if(p == null) return "unknown";
        StringBuilder sb = new StringBuilder();

        if(p.getDescription().getAuthors() == null) return "unknown";
        p.getDescription().getAuthors().forEach(Author -> sb.append(Author+" "));
        return sb.toString();
    }
    public static String getDependswithString(String plugin){
        Plugin p = Bukkit.getPluginManager().getPlugin(plugin);
        if(p == null) return "unknown";
        if(p.getDescription().getDepend() == null) return null;
        if(!AntiCheat.names().contains(plugin)) return null;
        StringBuilder sb = new StringBuilder();
        List<String> depend =p.getDescription().getDepend();
        List<String> softdepend = p.getDescription().getSoftDepend();
        ArrayList<String> sup = new ArrayList<>();
        for(AntiCheat ac : AntiCheat.values()){
            if(depend.contains(ac.getName().get(0))){
                sup.add(ac.getName().get(0));
            }
            if(softdepend.contains(ac.getName().get(0))){
                sup.add(ac.getName().get(0));
            }
        }
        int i = 0;
        for(String s:sup){
            i++;
            if(i != sup.size()){
                sb.append(s+", ");
            }else {
                sb.append(s);
            }
        }
        if(sup.size() == 0) return null;
        return sb.toString();
    }
    public static List<String> getDepends(String plugin){
        Plugin p = Bukkit.getPluginManager().getPlugin(plugin);
        if(p == null) return null;
        List<String> depend =p.getDescription().getDepend();
        List<String> softdepend = p.getDescription().getSoftDepend();
        ArrayList<String> sup = new ArrayList<>();
        for(AntiCheat ac : AntiCheat.values()){
            if(depend.contains(ac.getName().get(0))){
                sup.add(ac.getName().get(0));
            }
            if(softdepend.contains(ac.getName().get(0))){
                sup.add(ac.getName().get(0));
            }
        }
        if(sup.size() == 0) return null;
        return sup;
    }
}
