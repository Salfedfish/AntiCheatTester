package cn.rmc.act.util;

import cn.rmc.act.PlayerData;
import cn.rmc.act.main;
import cn.rmc.act.manager.PlayerManager;
import me.rerere.matrix.api.MatrixAPIProvider;
import me.vagdedes.spartan.api.API;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

public class PermissionUtils {
    public static void addPermission(Player p,String permission){
        PlayerManager pm = main.getInstance().playerManager;
        PlayerData pd = pm.get(p);
        PermissionAttachment pa = pd.getPermission();
        pa.setPermission(permission,true);
    }
    public static void addPermission(Player p,String[] permission){
        PlayerManager pm = main.getInstance().playerManager;
        PlayerData pd = pm.get(p);
        PermissionAttachment pa = pd.getPermission();
        for(String s: permission)pa.setPermission(s,true);
    }
    public static void removePermission(Player p,String permission){
        PlayerManager pm = main.getInstance().playerManager;
        PlayerData pd = pm.get(p);
        PermissionAttachment pa = pd.getPermission();
        pa.unsetPermission(permission);
    }
    public static void reloadacpermission(Player p){
        MatrixAPIProvider.getAPI().reloadPermissionCache(p);
        API.reloadPermissionCache(p);
    }
}
