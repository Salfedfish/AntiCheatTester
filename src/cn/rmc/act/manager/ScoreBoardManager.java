package cn.rmc.act.manager;

import cn.rmc.act.PlayerData;
import cn.rmc.act.enums.AntiCheat;
import cn.rmc.act.main;
import cn.rmc.act.util.ScoreboardUtil;
import org.bukkit.entity.Player;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ScoreBoardManager {
    public static void send(Player p){
        PlayerData pd = main.getInstance().playerManager.get(p);
        AntiCheat[] acs = pd.getAc();
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for(AntiCheat acc : acs){
            i++;
            if(i != acs.length){
                sb.append(acc.getName().get(acc.getName().size()-1)+", ");
            }else {
                sb.append(acc.getName().get(acc.getName().size()-1));
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yy/MM/dd");
        String format = sdf.format(new Date());
        List<String> score = Arrays.asList(
                "§e反作弊测试服",
                "§7" + format,
                "§r",
                "AntiCheat: §c" + sb,
                "CPS: Soon",
                "§r§r",
                "Flag: " + (pd.getFlag() ? "§atrue":"§cfalse"),
                "Kick: " + (pd.getKick() ? "§atrue":"§cfalse"),
                "Hungry: " + (pd.getHungry() ? "§atrue":"§cfalse"),
                "Damage: " + (pd.getDamage() ? "§atrue":"§cfalse"),
                "§r§r§r",
                "§emc.remiaft.com         "
        );
        ScoreboardUtil.SidebarDisplay(p,score.toArray(new String[0]));
    }
}
