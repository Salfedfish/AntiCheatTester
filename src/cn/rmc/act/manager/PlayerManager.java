package cn.rmc.act.manager;

import cn.rmc.act.PlayerData;
import cn.rmc.act.enums.AntiCheat;
import cn.rmc.act.event.AntiCheatUpdateEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

import java.util.HashMap;
import java.util.UUID;

public class PlayerManager {
    public HashMap<UUID, PlayerData> data = new HashMap<>();
    public PlayerData get(Player p){
        return data.get(p.getUniqueId());
    }
    public void setAntiCheat(Player p,AntiCheat[] ac){
        PermissionAttachment permission = get(p).getPermission();
        for(AntiCheat antiCheat : get(p).getAc()){
            permission.setPermission(antiCheat.getPermission(),true);
        }
        get(p).setAc(ac);
        for(AntiCheat antiCheat : get(p).getAc()){
            permission.unsetPermission(antiCheat.getPermission());
        }
        get(p).setPermission(permission);
        AntiCheatUpdateEvent event = new AntiCheatUpdateEvent(p,ac);
        Bukkit.getPluginManager().callEvent(event);
    }
    public void setAntiCheat(Player p,AntiCheat ac) {
        AntiCheat[] acsss = new AntiCheat[]{ac};
        AntiCheat[] acs = get(p).getAc();
        PermissionAttachment permission = get(p).getPermission();
        for (AntiCheat antiCheat : acs) {
            permission.setPermission(antiCheat.getPermission(), true);
        }
        get(p).setAc(acsss);
        permission.unsetPermission(ac.getPermission());
        AntiCheatUpdateEvent event = new AntiCheatUpdateEvent(p, acsss);
        Bukkit.getPluginManager().callEvent(event);
    }
}
