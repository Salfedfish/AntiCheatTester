package cn.rmc.act.listener;

import cn.rmc.act.PlayerData;
import cn.rmc.act.event.AntiCheatUpdateEvent;
import cn.rmc.act.main;
import cn.rmc.act.util.PermissionUtils;
import cn.rmc.act.util.TagUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class ACChangeListener implements Listener {
    @EventHandler
    public void onChange(AntiCheatUpdateEvent e){
        Player p = e.getPlayer();
        PlayerData pd = main.getInstance().playerManager.get(p);
        TagUtils.setTag(p,""," §7["+pd.acdisplay()+"]");
        Bukkit.getScheduler().runTaskLater(main.getInstance(),()->{
            PermissionUtils.reloadacpermission(p);
        },10L);
    }
}
