package cn.rmc.act.listener;

import cn.rmc.act.PlayerData;
import cn.rmc.act.main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class AntiCheatListener implements Listener {
    @EventHandler
    public void AACVL(me.konsolas.aac.api.PlayerViolationEvent e){
        Player p = e.getPlayer();
        String type = e.getHackType().getName();
        String mess = e.getMessage();
        int vl = e.getViolations();
        sendFlag(p,"AAC",type,vl,mess);
    }
    @EventHandler
    public void AACAPVL(de.photon.aacadditionpro.events.PlayerAdditionViolationEvent e){
        Player p = e.getPlayer();
        String type = e.getModuleType().name();
        String mess = e.getMessage();
        int vl = e.getVl();
        sendFlag(p,"AACAdditionPro",type,vl,mess);
    }
    @EventHandler
    public void MATRIXVL(me.rerere.matrix.api.events.PlayerViolationEvent e){
        Player p = e.getPlayer();
        String type = e.getHackType().name();
        String mess = e.getMessage();
        int vl = e.getViolations();
        sendFlag(p,"Matrix",type,vl,mess);
    }
    @EventHandler
    public void SpartanVL(me.vagdedes.spartan.api.PlayerViolationEvent e){
        Player p = e.getPlayer();
        String type = e.getHackType().name();
        String mess = e.getMessage();
        int vl = e.getViolation();
        sendFlag(p,"Spartan",type,vl,mess);
    }
    public static void sendFlag(Player p,String ac,String type,Integer vl,String message){
        PlayerData pd = main.getInstance().playerManager.get(p);
        if(!pd.getFlag()) return;
        p.sendMessage("§7[§3Flag§7] §8(" + ac +") §7"+p.getName()+" §ffailed §b" +type +"§f(vl: "+vl+"): §7"+message);
    }
}
