package cn.rmc.act.listener;

import cn.rmc.act.PlayerData;
import cn.rmc.act.enums.AntiCheat;
import cn.rmc.act.main;
import cn.rmc.act.manager.ScoreBoardManager;
import cn.rmc.act.menu.AntiCheatsMenu;
import cn.rmc.act.menu.ConfigMenu;
import cn.rmc.act.menu.SettingsMenu;
import cn.rmc.act.util.ItemBuilder;
import cn.rmc.act.util.PermissionUtils;
import cn.rmc.act.util.TagUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

public class PlayerListener implements Listener {
    ItemStack AntiCheatitem;
    ItemStack Configitem;

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(PlayerJoinEvent e){
        Player p = e.getPlayer();
        Inventory inv = p.getInventory();
        AntiCheatitem = new ItemBuilder(Material.NETHER_STAR).setName("§a切换反作弊 §7(右键点击)").toItemStack();
        Configitem = new ItemBuilder(Material.BOOK).setName("§a设置 §7(右键点击)").toItemStack();
        inv.setItem(7,AntiCheatitem);
        inv.setItem(8,Configitem);
        if(!main.getInstance().playerManager.data.containsKey(p.getUniqueId())){
            main.getInstance().playerManager.data.put(p.getUniqueId(),new PlayerData(p.getUniqueId()));
            PermissionUtils.addPermission(p, AntiCheat.permissions());
            PermissionUtils.removePermission(p,AntiCheat.AAC.getPermission());
        }else{
            AntiCheat[] before = main.getInstance().playerManager.get(p).getAc();
            main.getInstance().playerManager.get(p).setPermission(p.addAttachment(main.getInstance()));
            PermissionUtils.addPermission(p, AntiCheat.permissions());
            main.getInstance().playerManager.setAntiCheat(p,before);
        }
        ScoreBoardManager.send(p);
        PlayerData pd = main.getInstance().playerManager.get(p);
        TagUtils.setTag(p,""," §7["+pd.acdisplay()+"]");
        Bukkit.getScheduler().runTaskLater(main.getInstance(),()->{
            PermissionUtils.reloadacpermission(p);
        },20L);
        for(Player player : Bukkit.getOnlinePlayers()){
            PlayerData pda = main.getInstance().playerManager.get(p);
            TagUtils.setTag(player,""," §7["+pda.acdisplay()+"]");
            PermissionUtils.reloadacpermission(player);
        }
    }
    @EventHandler
    public void onQuit(PlayerQuitEvent e){
        Player p =e.getPlayer();
        TagUtils.unregisterTag(p);
    }
    @EventHandler
    public void onClickinv(InventoryClickEvent e){
        Player p = (Player) e.getWhoClicked();
        ItemStack clicked = e.getCurrentItem();
        switch (clicked.getType()){
            case NETHER_STAR:
                e.setCancelled(true);
            case BOOK:
                e.setCancelled(true);
            default:
        }
    }
    @EventHandler
    public void onint(PlayerInteractEvent e){
        try {
            if(e.getAction() != Action.RIGHT_CLICK_AIR && e.getAction() != Action.RIGHT_CLICK_BLOCK) return;
            String s = e.getItem().getItemMeta().getDisplayName();
            if(s.equals(AntiCheatitem.getItemMeta().getDisplayName())){
                AntiCheatsMenu.open(e.getPlayer());
            }
            if(s.equals(Configitem.getItemMeta().getDisplayName())){
                ConfigMenu.open(e.getPlayer());
            }
        }catch (NullPointerException exception){}
    }
    @EventHandler
    public void onDamage(EntityDamageEvent e){
        if(!(e.getEntity() instanceof Player))return;
        Player p = (Player) e.getEntity();
        if(main.getInstance().playerManager.get(p).getDamage()) return;
        e.setCancelled(true);
    }
    @EventHandler
    public void onKick(PlayerKickEvent e){
        if(main.getInstance().playerManager.get(e.getPlayer()).getKick()) return;
        e.setCancelled(true);
        e.getPlayer().sendTitle(ChatColor.RED + "你被反作弊踢出了游戏",e.getReason());
        e.getPlayer().sendMessage(new String[]{ChatColor.RED + "你被反作弊踢出了游戏",e.getReason()});
    }

    @EventHandler
    public void onHungry(FoodLevelChangeEvent e){
        if(!(e.getEntity() instanceof Player)) return;
        Player p = (Player) e.getEntity();
        PlayerData pd = main.getInstance().playerManager.get(p);
        if(pd.getHungry()) return;
        p.setFoodLevel(20);
        e.setCancelled(true);
    }
}
