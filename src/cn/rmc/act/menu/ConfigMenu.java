package cn.rmc.act.menu;

import cn.rmc.act.main;
import cn.rmc.act.util.ItemBuilder;
import cn.rmc.act.util.inventory.InventoryUI;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

public class ConfigMenu {
    private InventoryUI ConfigMenus;
    public ConfigMenu(){
        setup();
    }
    public void setup(){
        ConfigMenus = new InventoryUI("AntiCheat",4);
        //ACCHOOSE
        ConfigMenus.setItem(12, new InventoryUI.AbstractClickableItem(new ItemBuilder(Material.COMMAND).setName("§a反作弊选择").toItemStack()) {
            @Override
            public void onClick(InventoryClickEvent e) {
                AntiCheatsMenu.open((Player)e.getWhoClicked());
            }
        });
        //Setting
        ConfigMenus.setItem(14, new InventoryUI.AbstractClickableItem(new ItemBuilder(Material.REDSTONE_COMPARATOR).setName("§a设置").toItemStack()) {
            @Override
            public void onClick(InventoryClickEvent e) {
                SettingsMenu.open((Player) e.getWhoClicked());
            }
        });


    }
    public static void open(Player p){
        p.openInventory(main.getInstance().configMenu.ConfigMenus.getCurrentPage());
    }
}
