package cn.rmc.act.menu;


import cn.rmc.act.enums.AntiCheat;
import cn.rmc.act.main;
import cn.rmc.act.util.ItemBuilder;
import cn.rmc.act.util.PluginInfoUtils;
import cn.rmc.act.util.inventory.InventoryUI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;

public class AntiCheatsMenu {
    private InventoryUI acchoose;
    public AntiCheatsMenu(){
        setup();
    }
    public void setup(){
        acchoose = new InventoryUI("AntiCheat",2);
        acchoose.setItem(0, AItem(Material.GRASS,"AAC"));
        acchoose.setItem(1,AItem(Material.SAPLING,"AACAdditionPro"));
        acchoose.setItem(2,AItem(Material.NETHER_STAR,"Matrix"));
        acchoose.setItem(3,AItem(Material.BOOK,"Spartan"));


    }
    public static void open(Player p){
        p.openInventory(main.getInstance().antiCheatChoose.acchoose.getCurrentPage());
    }
    public static ItemStack ACItem(Material material,String plugin){
        ItemBuilder ib = new ItemBuilder(material,1);
        ib.setName(ChatColor.YELLOW + plugin);
        ib.addLoreLine(" ").
                addLoreLine("§7Version: " + PluginInfoUtils.getVersion(plugin)).
                addLoreLine("§7Author/s: " + PluginInfoUtils.getAuthor(plugin));
        if(PluginInfoUtils.getDepends(plugin) != null){
            ib.addLoreLine(" ").
                    addLoreLine("§7Depend: " + PluginInfoUtils.getDependswithString(plugin));
        }
        return ib.toItemStack();
    }
    public static InventoryUI.AbstractClickableItem AItem(Material material,String plugin){
        ItemStack ac = ACItem(material,plugin);
        InventoryUI.AbstractClickableItem result = new InventoryUI.AbstractClickableItem(ac) {
            @Override
            public void onClick(InventoryClickEvent e) {
                ArrayList<AntiCheat> setter = new ArrayList<>();
                if(PluginInfoUtils.getDepends(plugin) != null){
                    for(String s:PluginInfoUtils.getDepends(plugin)){
                        setter.add(AntiCheat.getACByName(s));
                    }
                }
                setter.add(AntiCheat.getACByName(plugin));
                StringBuilder sb = new StringBuilder();
                int i = 0;
                for(cn.rmc.act.enums.AntiCheat acc : setter){
                    i++;
                    if(i != setter.size()){
                        sb.append(acc.getName().get(acc.getName().size()-1)+", ");
                    }else {
                        sb.append(acc.getName().get(acc.getName().size()-1));
                    }
                }
                main.getInstance().playerManager.setAntiCheat((Player)e.getWhoClicked(), setter.toArray(new AntiCheat[0]));
                e.getWhoClicked().sendMessage("你已设置反作弊: "+ main.getInstance().playerManager.get((Player) e.getWhoClicked()).acdisplay());
                e.getWhoClicked().closeInventory();
            }
        };
        return result;
    }
}
