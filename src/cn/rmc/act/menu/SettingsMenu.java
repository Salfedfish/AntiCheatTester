package cn.rmc.act.menu;

import cn.rmc.act.PlayerData;
import cn.rmc.act.enums.Settings;
import cn.rmc.act.main;
import cn.rmc.act.util.ItemBuilder;
import cn.rmc.act.util.inventory.InventoryUI;
import cn.rmc.act.util.inventory.ItemUtil;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;

public class SettingsMenu {
    private InventoryUI SettingsMenu;
    private HashMap<UUID,InventoryUI> data = new HashMap<>();
    public SettingsMenu(){
        setup();
    }
    public void setup(){
        SettingsMenu = new InventoryUI("Settings",4);
        //Flag
        SettingsMenu.setItem(10, new InventoryUI.AbstractClickableItem(
                new ItemBuilder(Material.STAINED_GLASS_PANE,1,(byte) 14).setName("§bFlag").toItemStack()) {
            @Override
            public void onClick(InventoryClickEvent e) {
                Player p = (Player) e.getWhoClicked();
                PlayerData pd = main.getInstance().playerManager.get(p);
                Boolean booleans = pd.getFlag();
                if (booleans) {
                    pd.setFlag(false);
                } else {
                    pd.setFlag(true);
                }
                update(p,Settings.FLAG);
            }
        });
        //Kick
        SettingsMenu.setItem(12, new InventoryUI.AbstractClickableItem(
                new ItemBuilder(Material.ANVIL).setName("§bKick").toItemStack()) {
            @Override
            public void onClick(InventoryClickEvent e) {
                Player p = (Player) e.getWhoClicked();
                PlayerData pd = main.getInstance().playerManager.get(p);
                Boolean booleans = pd.getKick();
                if (booleans) {
                    pd.setKick(false);
                } else {
                    pd.setKick(true);
                }
                update(p,Settings.KICK);
            }
        });
        //Hungry
        SettingsMenu.setItem(14, new InventoryUI.AbstractClickableItem(
                new ItemBuilder(Material.COOKED_BEEF).setName("§bHungry").toItemStack()) {
            @Override
            public void onClick(InventoryClickEvent e) {
                Player p = (Player) e.getWhoClicked();
                PlayerData pd = main.getInstance().playerManager.get(p);
                Boolean booleans = pd.getHungry();
                if (booleans) {
                    pd.setHungry(false);
                } else {
                    pd.setHungry(true);
                }
                p.setFoodLevel(20);
                update(p,Settings.HUNGRY);
            }
        });
        //Damage
        SettingsMenu.setItem(16, new InventoryUI.AbstractClickableItem(
                new ItemBuilder(Material.REDSTONE).setName("§bDamage").toItemStack()) {
            @Override
            public void onClick(InventoryClickEvent e) {
                Player p = (Player) e.getWhoClicked();
                PlayerData pd = main.getInstance().playerManager.get(p);
                Boolean booleans = pd.getDamage();
                if (booleans) {
                    pd.setDamage(false);
                } else {
                    pd.setDamage(true);
                }
                update(p,Settings.DAMAGE);
                p.setHealth(20.0);
            }
        });
    }
    public void update(Player p,Settings settings){
        InventoryUI inventoryUI;
        if(data.containsKey(p.getUniqueId())){
            inventoryUI = data.get(p.getUniqueId());
        }else{
            inventoryUI = SettingsMenu;
        }
        PlayerData pd = main.getInstance().playerManager.get(p);
        InventoryUI.ClickableItem item = null;
        int  i = 0;
        Boolean booleans = null;
        switch (settings){
            case FLAG:
                i = 10;
                item = inventoryUI.getItem(i);
                booleans = pd.getFlag();
                break;
            case KICK:
                i = 12;
                item = inventoryUI.getItem(i);
                booleans = pd.getKick();
                break;
            case HUNGRY:
                i = 14;
                item = inventoryUI.getItem(i);
                booleans = pd.getHungry();
                break;
            case DAMAGE:
                i = 16;
                item = inventoryUI.getItem(i);
                booleans = pd.getDamage();
                break;
        }
        ItemStack itemStack = item.getItemStack();
        ItemMeta itemmeta = itemStack.getItemMeta();
        ItemUtil.hideEnchants(itemStack);
        if(booleans){
            if(!itemmeta.hasEnchants()){
                ItemUtil.enchantItem(itemStack, new ItemUtil.ItemEnchant(Enchantment.DURABILITY,1));
            }
        }else{
            if(itemmeta.hasEnchants()){
                itemmeta.removeEnchant(Enchantment.DURABILITY);
                itemStack.setItemMeta(itemmeta);
            }
        }
        item.setItemStack(ItemUtil.reloreItem(item.getItemStack(),"§7点击切换","§7当前状态: "+(booleans ? "§atrue":"§cfalse")));
        inventoryUI.setItem(i,item);
        data.put(p.getUniqueId(),inventoryUI);
    }
    public static void open(Player p){
        for(Settings settings : Settings.values()){
            main.getInstance().settingsMenu.update(p,settings);
        }
        p.openInventory(main.getInstance().settingsMenu.data.get(p.getUniqueId()).getCurrentPage());
    }
}
