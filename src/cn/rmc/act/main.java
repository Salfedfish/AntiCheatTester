package cn.rmc.act;

import cn.rmc.act.command.AntiCheatCommand;
import cn.rmc.act.command.ConfigCommand;
import cn.rmc.act.command.StatusCommand;
import cn.rmc.act.listener.ACChangeListener;
import cn.rmc.act.listener.AntiCheatListener;
import cn.rmc.act.listener.PlayerListener;
import cn.rmc.act.listener.WorldListener;
import cn.rmc.act.manager.PlayerManager;
import cn.rmc.act.menu.AntiCheatsMenu;
import cn.rmc.act.menu.ConfigMenu;
import cn.rmc.act.menu.SettingsMenu;
import cn.rmc.act.util.TagUtils;
import cn.rmc.act.util.inventory.UIListener;
import cn.rmc.act.runnable.ScoreBoardRunnable;
import net.minecraft.server.v1_8_R3.MinecraftServer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;


public class main extends JavaPlugin {
    public PlayerManager playerManager;
    public static main instance;
    public AntiCheatsMenu antiCheatChoose;
    public ConfigMenu configMenu;
    public SettingsMenu settingsMenu;
    public static main getInstance(){
        return instance;
    }
    @Override
    public void onEnable() {
        this.instance = this;
        antiCheatChoose = new AntiCheatsMenu();
        configMenu = new ConfigMenu();
        settingsMenu = new SettingsMenu();
        playerManager = new PlayerManager();
        Bukkit.getScheduler().runTaskTimer(this,new ScoreBoardRunnable(),0L,10L);
        registerEvents();
        registerCommands();
    }

    @Override
    public void onDisable() {
        TagUtils.unregisterAll();
    }

    void registerEvents(){
        System.out.println("Listener Registered!");
        Arrays.asList(new ACChangeListener(),
                new AntiCheatListener(),
                new PlayerListener(),
                new WorldListener(),
                new UIListener()
        ).forEach(listener -> Bukkit.getPluginManager().registerEvents(listener,this));
    }
    void registerCommands(){
        registerCommand(new AntiCheatCommand());
        registerCommand(new StatusCommand());
        registerCommand(new ConfigCommand());
    }


    private void registerCommand(final Command cmd) {
        MinecraftServer.getServer().server.getCommandMap().register(cmd.getName(), this.getName(), cmd);
    }
}
