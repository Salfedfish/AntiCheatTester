package cn.rmc.act.runnable;

import cn.rmc.act.manager.ScoreBoardManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ScoreBoardRunnable implements Runnable {

    @Override
    public void run() {
        for(Player p: Bukkit.getOnlinePlayers()){
            ScoreBoardManager.send(p);
        }
    }
}
