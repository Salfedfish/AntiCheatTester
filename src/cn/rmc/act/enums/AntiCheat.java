package cn.rmc.act.enums;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum AntiCheat {
    VANILLA(Arrays.asList("Vanilla"),"Vanilla.bypass"),
    AAC(Arrays.asList("AAC"),"AAC.bypass"),
    AACADDITIONPRO(Arrays.asList("AACAdditionPro","AACAP","AACP"),"aacadditionpro.bypass.*"),
    MATRIX(Arrays.asList("Matrix"),"Matrix.bypass"),
    SPARTAN(Arrays.asList("Spartan"),"Spartan.bypass"),
    REFLEX(Arrays.asList("Reflex"),"Reflex.bypass");

    @Getter
    private String permission;
    @Getter
    private List<String> name;
    public static AntiCheat getACByName(String name){
        AntiCheat result = null;
        for(AntiCheat ac: values()){
            for(String s : ac.name){
                if(s.equalsIgnoreCase(name)){
                    result = ac;
                    break;
                }
            }
        }
        return result;
    }
    public static String[] permissions(){
        ArrayList<String> result = new ArrayList<>();
        for(AntiCheat ac :AntiCheat.values()){
            result.add(ac.permission);
        }
        return result.toArray(new String[0]);
    }
    public static ArrayList<String> names(){
        ArrayList<String> result = new ArrayList<>();
        for(AntiCheat ac:AntiCheat.values()){
            result.add(ac.name.get(0));
        }
        return result;
    }
    AntiCheat(List<String> name, String permission){
        this.name = name;
        this.permission = permission;
    }

}

