package cn.rmc.act.command;

import cn.rmc.act.enums.AntiCheat;
import cn.rmc.act.main;
import cn.rmc.act.menu.AntiCheatsMenu;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;


public class AntiCheatCommand extends Command {


    public AntiCheatCommand() {
        super("anticheat");
        this.setAliases(Arrays.asList("ac"));
    }

    @Override
    public boolean execute(CommandSender sender, String s, String[] args) {
        if(!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        if(args.length == 0){
            AntiCheatsMenu.open(p);
        }
        if(args.length >= 1){
            ArrayList<AntiCheat> setter = new ArrayList<>();

            for(String acc : args){
                if(AntiCheat.getACByName(acc) != null) {
                    AntiCheat ac =AntiCheat.getACByName(acc);
                    if(setter.contains(ac)) continue;
                    if(ac == AntiCheat.AACADDITIONPRO){
                        if(!setter.contains(AntiCheat.AAC)){
                            setter.add(AntiCheat.AAC);
                        }
                    }
                    setter.add(AntiCheat.getACByName(acc));
                }
            }
            if(setter.size() == 0) return true;
            AntiCheat[] acs = setter.toArray(new AntiCheat[0]);
            main.getInstance().playerManager.setAntiCheat(p,acs);
            p.sendMessage("你已设置反作弊: "+main.getInstance().playerManager.get(p).acdisplay());
        }
        return false;
    }
}
