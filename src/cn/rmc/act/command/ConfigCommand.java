package cn.rmc.act.command;

import cn.rmc.act.menu.ConfigMenu;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ConfigCommand extends Command {
    public ConfigCommand(){
        super("config");
    }

    @Override
    public boolean execute(CommandSender sender, String s, String[] args) {
        if(!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        ConfigMenu.open(p);
        return false;
    }
}
