package cn.rmc.act.command;

import cn.rmc.act.PlayerData;
import cn.rmc.act.enums.AntiCheat;
import cn.rmc.act.main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StatusCommand extends Command {
    public StatusCommand() {
        super("status");
    }

    @Override
    public boolean execute(CommandSender sender, String s, String[] args) {
        if(!(sender instanceof Player)) return true;
        Player p = (Player) sender;
        if(args.length == 0){
            PlayerData pd =main.getInstance().playerManager.get(p);
            StringBuilder acc = new StringBuilder();
            for(AntiCheat ac:pd.getAc()){
                acc.append(ac.name()+" ");
            }
            StringBuilder permi = new StringBuilder();
            for(String ss : pd.getPermission().getPermissions().keySet()){
                permi.append(ss+" ");
            }
            p.sendMessage(new String[]{
                    "" + acc,
                    "" + pd.getFlag(),
                    "" + permi
            });
            return true;
        }
        if(args.length == 1){

        }
        return false;
    }
}
